import { SessionHelper } from "./session";


// export const getBase64Url = (file) => {
//     var reader = new FileReader();
//     reader.readAsDataURL(file);
//     var base64 = new Promise(function (resolve, reject) {
//       reader.onload = (event) => {
//         if (event.target)
//           resolve(event.target.result);
//         else
//           reject(new Error('fail'))
//       };
//     });
//     return base64;
//   }

  export const guardMyroute = (to, from, next) => {
    if(SessionHelper.getCookie('token') === ''){
      next({name: 'login'})
    }else{
      next()
    }
  }

  export const  getFormData = (obj) => {
    console.log('=obj=obj=getFormData', obj)
    var formData = new FormData();
    for (var key in obj) {
        formData.append(key, obj[key]);
    }
    return formData;
}

export const toast = ($this, message, type) => {
	let msgTypes = ['success','error'];
    $this.$toast.open({
        message: message,
        type: msgTypes.includes(type) ? type : 'default',
        duration: 3500,
        dismissible: true,
        queue: true,
        position: 'top'
    })
}