import Vue from 'vue'
import Home from './views/Home'
import './registerServiceWorker'
import router from './router'
import store from './store'
import App from './App'
import './assets/style.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import { BCard, BFormCheckbox, BCardGroup, BCardText, BFormSelect, BTable, BPagination, BSpinner, BFormTextarea, ModalPlugin, BModal	 } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.component('b-form-select', BFormSelect)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
